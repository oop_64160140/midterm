package com.panneerat.midterm;

public class TestSudent {
    public static void main(String[] args) {
        Student Student1 = new Student("Panneerat", "64160140", 40, 46.9);
        Student Student2 = new Student("Mark Lee", "64160000", 39.9, 48.5);
        Student Student3 = new Student("Jaehyun", "64160999", 30.5, 43.7);
        Student Student4 = new Student("NuNu", "64160111", 26.2, 33.1);
        Student Student5 = new Student("Johnny", "64160404", 44, 29.9);
        Student Student6 = new Student("Kam", "64160100", 45.3, 31);

        Student1.calculateTotal();
        Student1.print();

        Student2.calculateTotal();
        Student2.print();

        Student3.calculateTotal();
        Student3.print();

        Student4.calculateTotal();
        Student4.print();

        Student5.calculateTotal();
        Student5.print();

        Student6.calculateTotal();
        Student6.print();
    }
}
