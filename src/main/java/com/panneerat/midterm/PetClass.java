package com.panneerat.midterm;

public class PetClass {
    private String pet;
    private String name;
    private String breed;
    private int age;

    public PetClass(String pet, String name, int age, String breed){
        this.pet = pet;
        this.name = name;
        this.age = age;
        this.breed = breed;
    }

    public String getPet(){
        return pet;
    }

    public String getName(){
        return name;
    }

    public String getBreed(){
        return breed;
    }

    public int getAge(){
        return age;
    }


    public String Eat(){
        return "กำลังหิว!";
    }

    public String Walk(){
        return "กำลังเดิน... ";
    }
    
    public String Sleep(){
        return "กำลังนอน ZzzZ";
    }
   

    public void print(){
        System.out.println(" Pet: "+ pet);
        System.out.println(" Name: "+ name);
        System.out.println(" Age: "+ age + " yeas ");
        System.out.println(" Breed: "+ breed);
    }

    public void printSeeWalk(){
        System.out.println(" Behavior(พฤติกรรม): "+ Walk());
        System.out.println("----------------");
    }

    public void printEat(){
        System.out.println(" Behavior(พฤติกรรม): "+ Eat());
        System.out.println("----------------");
    }

    public void printSleep(){
        System.out.println(" Behavior(พฤติกรรม): "+ Sleep());
        System.out.println("----------------");
    }
}

