package com.panneerat.midterm;

public class TestPet {
public static void main(String[] args) {

    PetClass Pet1 = new PetClass("Cat", "Leeon", 2, "วิเชียรมาศ");
    PetClass Pet2 = new PetClass("Dog", "Milo", 3, "Pomeranian");
    PetClass Pet3 = new PetClass("Cat", "Dum", 10, "Thai");
    PetClass Pet4 = new PetClass("Duck", "Don", 7, "Duck");
    PetClass Pet5 = new PetClass("Dog", "Mali", 3, "Chihuahua");
    PetClass Pet6 = new PetClass("Cat", "Bow", 3, "Scottish Fold");
   
    Pet1.print();
    Pet1.printEat();
    

    Pet2.print();
    Pet2.printSeeWalk();
    

    Pet3.print();
    Pet3.printEat();

 
    Pet4.print();
    Pet4.printSeeWalk();

 
    Pet5.print();
    Pet5.printSleep();
    

    Pet6.print();
    Pet6.printEat();
    
   }
}