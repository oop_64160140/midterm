package com.panneerat.midterm;

public class Student {
    private String name;
    private String ID;
    private double totalmidterm;
    private double totalfinal;

    public Student(String name, String ID, double totalmidterm, double totalfinal){
        this.name = name;
        this.ID = ID;
        this.totalmidterm = totalmidterm;
        this.totalfinal = totalfinal;
    }

    public String getName(){
        return name;
    }

    public String getID(){
        return ID;
    }

    public double calculateTotal(){
        double total = totalmidterm + totalfinal;
        return total;
    }

    public void print(){
        System.out.println("ชื่อ: " + name);
        System.out.println("รหัสนิสิต: " + ID);
        System.out.println("คะแนน: " + calculateTotal());
        System.out.println("------------");
    }

}
